import { InstrumentData } from "./instrument-data";
import { SongData } from "./song-data";

export interface TrackData {
    tracks: (SongData | null)[]
    macro: Record<string, string>
    instruments: InstrumentData[]
    instrumentNameMap: Map<string, number>
    spc: {
        song: string
        game: string
        artist: string
        comment: string
        length: number
        fadeout: number
    }
}
