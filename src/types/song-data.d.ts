export type SongData =
    SongNoteData |
    SongHexCommandData |
    SongInstrumentData |
    SongVolumeData |
    SongGlobalVolumeData |
    SongPanData |
    SongTempoData |
    SongVibratoData |
    SongLoopPointData |
    SongRepeatStartData |
    SongRepeatEndData;

export interface SongNoteData {
    type: "note"
    noteParam: number
    pitch: number
    length: number
}

export interface SongHexCommandData {
    type: "hex-command"
    param: number
}

export interface SongInstrumentData {
    type: "instrument"
    param: number
}

export interface SongVolumeData {
    type: "volume"
    param: number
}

export interface SongGlobalVolumeData {
    type: "global-volume"
    param: number
}

export interface SongPanData {
    type: "pan"
    param: number
}

export interface SongTempoData {
    type: "tempo"
    param: number
}

export interface SongVibratoData {
    type: "vibrato"
    param: number[]
}

export interface SongLoopPointData {
    type: "loop-point"
}

export interface SongRepeatStartData {
    type: "repeat-start"
}

export interface SongRepeatEndData {
    type: "repeat-end"
    param: number
}
