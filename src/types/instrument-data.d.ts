export interface InstrumentData {
    fileName: string
    customName: string
    param: number[]
}
