import * as console from "console";
import type { SongData, SongNoteData } from "../types/song-data";

type NotePitch = {
    [key: string]: number
};

const notePitch: NotePitch = {
    c: 0,
    d: 2,
    e: 4,
    f: 5,
    g: 7,
    a: 9,
    b: 11,
    r: -1,
    "^": -2,
};

const hexRegex = /^[0-9a-fA-F]{2}$/;

export interface SongReaderOptions {
    allowSwitchTrack?: boolean
    allowReplace?: boolean
    replaceMap?: Record<string, string>
    barSize?: number
}

export default function songReader(inputText: string, options: SongReaderOptions = {}) {
    const octaveMin = 1;
    const octaveMax = 6;
    const barSize = options.barSize ?? 192;

    let text = inputText;
    let pos = 0;
    let tuning = 0;
    let octave = 4;
    let noteParam = 0x7f;
    let channel: string | number = 0;
    const ticks = new Map<(number | string), number>();
    const introTicks = new Map<(number | string), number>();
    let ticksHold = 0;
    let repeatingBucket = false;
    const defaultLength = 8;
    const allData = new Map<(number | string), SongData[]>();
    let data: SongData[] = [];
    allData.set(0, data);

    function char() {
        return text[pos];
    }

    function next(step = 1) {
        pos += step;
    }

    function skipEmpty() {
        while (/^\s$/g.test(char())) {
            next();
        }
    }

    function error(reason: string): never {
        const lines = text.slice(0, pos).split("\n");
        const line = lines.length;
        const ch = lines[lines.length - 1].length;
        throw new SyntaxError(`At line ${line}, char ${ch}: ${reason}`);
    }

    function skipComment(multiline = false) {
        if (!multiline) {
            next();
            if (char() !== "/") {
                error("bad syntax");
            }
            next();
            while (char() !== "\n") {
                next();
            }
            return;
        }
        error("multiline comment is not implemented");
    }

    function fetchNumber(mayNegative = false) {
        let content = "";
        if (mayNegative) {
            if (char() === "-" || char() === "+") {
                content += char();
                next();
            }
        }
        while (char() >= "0" && char() <= "9") {
            content += char();
            next();
        }
        if (content === "") {
            return null;
        }
        return Number(content);
    }

    function fetchHex() {
        let content = "";
        content += char();
        next();
        content += char();
        next();
        if (!hexRegex.test(content)) {
            error("Not a valid hex number");
        }
        return Number(parseInt(content, 16));
    }

    function fetchBin() {
        let content = "";
        for (let i = 0; i < 8; i++) {
            content += char();
            next();
        }
        if (!/^[01]{8}$/.test(content)) {
            return null;
        }
        return Number(parseInt(content, 2));
    }

    function fetchNote() {
        const songData: SongNoteData = {
            type: "note",
            length: defaultLength,
            pitch: 0,
            noteParam,
        };
        let abs = false;
        songData.length = defaultLength;
        songData.pitch = notePitch[char()];
        next();
        if (char() === "+" || char() === "-") {
            if (songData.pitch < 0) {
                error("Sharp/Flat mark can't be used with a tie or rest");
            }
            songData.pitch += (char() === "+") ? 1 : -1;
            next();
        }
        if (char() === "=") {
            abs = true;
            next();
        }
        const len = fetchNumber();
        if (len !== null) {
            /* if (len > barSize) {
                error('Invalid note length');
            } */
            if (abs) {
                songData.length = len;
            } else if (barSize % len === 0) {
                songData.length = barSize / len;
            } else {
                error(`${barSize} is not divisible by ${len}`);
            }
        }
        let oldLen = songData.length;
        while (char() === ".") {
            if (oldLen % 2 !== 0) {
                error("Note length is divided too much");
            }
            songData.length += Math.floor(oldLen / 2);
            oldLen /= 2;
            next();
        }
        if (songData.pitch >= 0) {
            const newPitch = octave * 12 + songData.pitch + tuning;
            if (newPitch < 0 || newPitch >= (octaveMax + 1) * 12) {
                error("Pitch out of bound");
            }
            songData.pitch = newPitch;
        }
        /* if (data.length > 0 && songData.pitch === -2) {
            data[data.length - 1].length += songData.length;
            return;
        } */
        if (repeatingBucket) {
            ticksHold += songData.length;
        } else {
            ticks.set(channel, (ticks.get(channel) || 0) + songData.length);
        }
        data.push(songData);
        // return songData;
    }

    function fetchTuning() {
        next();
        const tuningRaw = fetchNumber(true);
        if (tuningRaw === null) {
            error("Invalid tuning value");
        }
        tuning = tuningRaw;
    }

    function fetchOctave() {
        next();
        const octaveRaw = fetchNumber();
        if (octaveRaw === null || (octaveRaw < octaveMin || octaveRaw > octaveMax)) {
            error("Invalid octave value");
        }
        octave = octaveRaw;
    }

    function fetchOctaveStep() {
        let newOctave = octave;
        if (char() === ">") {
            newOctave += 1;
        } else if (char() === "<") {
            newOctave -= 1;
        }
        if (newOctave < octaveMin && newOctave > octaveMax) {
            error("Octave goes out of bound");
        }
        octave = newOctave;
        next();
    }

    function fetchNoteParameters() {
        next();
        noteParam = fetchHex();
    }

    function fetchInstrument() {
        next();
        const instrument = fetchNumber();
        if (instrument === null || instrument < 0 || instrument > 255) {
            error("Invalid instrument ID");
        }
        data.push({
            type: "instrument",
            param: instrument,
        });
    }

    function fetchVolume() {
        next();
        const volume = fetchNumber();
        if (volume === null || volume < 0 || volume > 255) {
            error("Invalid volume");
        }
        data.push({
            type: "volume",
            param: volume,
        });
    }

    function fetchGlobalVolume() {
        next();
        const globalVolume = fetchNumber();
        if (globalVolume === null || globalVolume < 0 || globalVolume > 255) {
            error("Invalid global volume");
        }
        data.push({
            type: "global-volume",
            param: globalVolume,
        });
    }

    function fetchPan() {
        next();
        const pan = fetchNumber();
        if (pan === null || pan < 0 || pan > 255) {
            error("Invalid pan");
        }
        data.push({
            type: "pan",
            param: pan,
        });
    }

    function fetchTempo() {
        next();
        const tempo = fetchNumber();
        if (tempo === null || tempo < 0 || tempo > 255) {
            error("Invalid pan");
        }
        data.push({
            type: "tempo",
            param: tempo,
        });
    }

    function fetchVibrato() {
        next();
        const arg1 = fetchNumber();
        if (arg1 === null || arg1 < 0 || arg1 > 255) {
            error("Invalid tempo value");
        }
        skipEmpty();
        if (char() !== ",") {
            error("Invalid tempo value (not enough arguments)");
        }
        next();
        skipEmpty();
        const arg2 = fetchNumber();
        if (arg2 === null || arg2 < 0 || arg2 > 255) {
            error("Invalid tempo value");
        }
        skipEmpty();
        if (char() !== ",") {
            data.push({
                type: "vibrato",
                param: [0, arg1, arg2],
            });
            return;
        }
        next();
        skipEmpty();
        const arg3 = fetchNumber();
        if (arg3 === null || arg3 < 0 || arg3 > 255) {
            error("Invalid tempo value");
        }
        data.push({
            type: "vibrato",
            param: [arg1, arg2, arg3],
        });
    }

    function fetchHexCommand() {
        next();
        const command = fetchHex();
        data.push({
            type: "hex-command",
            param: command,
        });
    }

    function fetchHexCommandDecimal() {
        next();
        const command = fetchNumber(true);
        if (command === null || command < -128 || command > 255) {
            error("invalid decimal value");
        }
        data.push({
            type: "hex-command",
            param: command < 0 ? 256 + command : command,
        });
    }

    function fetchHexCommandBinary() {
        next();
        const command = fetchBin();
        if (command === null) {
            error("invalid binary value");
        }
        data.push({
            type: "hex-command",
            param: command,
        });
    }

    function fetchLoopPoint() {
        if (repeatingBucket) {
            error("Repeating bucket must be exited before declaring loop point");
        }
        next();
        data.push({
            type: "loop-point",
        });
        introTicks.set(channel, ticks.get(channel) || 0);
    }

    function fetchRepeatStart() {
        if (repeatingBucket) {
            error("repeating bucket can't be nested");
        }
        next();
        data.push({
            type: "repeat-start",
        });
        repeatingBucket = true;
    }

    function fetchRepeatEnd() {
        if (!repeatingBucket) {
            error("not inside repeating bucket");
        }
        next();
        const param = fetchNumber();
        if (param === null || param < 2 || param > 255) {
            error(`invalid repeat time: ${param}`);
        }
        data.push({
            type: "repeat-end",
            param,
        });
        ticks.set(channel, (ticks.get(channel) || 0) + ticksHold * param);
        repeatingBucket = false;
        ticksHold = 0;
    }

    function fetchReplace() {
        next();
        if (char() !== "(") {
            error("macro name must be wrapped with ASCII brackets");
        }
        next();
        let replaceName = "";
        while (char() !== ")") {
            if (char() === "\\") {
                next();
            }
            replaceName += char();
            next();
        }
        const replaceTo = options.replaceMap?.[replaceName] ?? "";
        console.log(`${replaceName} => ${replaceTo}`);
        next();
        text = `${text.slice(0, pos)}${replaceTo}${text.slice(pos)}`;
    }

    function switchTrack() {
        if (!options.allowSwitchTrack) {
            error("track switching is disabled in this context");
        }
        if (repeatingBucket) {
            error("Repeating bucket must be exited before switching track");
        }
        next();
        const trackId = fetchNumber();
        if (trackId === null) {
            error("unexpected track ID");
        }
        data = allData.get(trackId) || [];
        channel = trackId;
        allData.set(trackId, data);
    }

    while (pos < text.length - 1) {
        if (/^([a-g^r])$/.test(char())) {
            fetchNote();
            continue;
        }
        if (char() === "h") {
            fetchTuning();
            continue;
        }
        if (char() === "o") {
            fetchOctave();
            continue;
        }
        if (char() === "q") {
            fetchNoteParameters();
            continue;
        }
        if (char() === "v") {
            fetchVolume();
            continue;
        }
        if (char() === "w") {
            fetchGlobalVolume();
            continue;
        }
        if (char() === "y") {
            fetchPan();
            continue;
        }
        if (char() === "t") {
            fetchTempo();
            continue;
        }
        if (char() === "p") {
            fetchVibrato();
            continue;
        }
        if (char() === "@") {
            fetchInstrument();
            continue;
        }
        if (char() === "<" || char() === ">") {
            fetchOctaveStep();
            continue;
        }
        if (char() === "/") {
            skipComment();
            continue;
        }
        if (char() === "!") {
            fetchLoopPoint();
            continue;
        }
        if (char() === "[") {
            fetchRepeatStart();
            continue;
        }
        if (char() === "]") {
            fetchRepeatEnd();
            continue;
        }
        if (char() === "#") {
            switchTrack();
            continue;
        }
        if (char() === "X") {
            fetchHexCommand();
            continue;
        }
        if (char() === "D") {
            fetchHexCommandDecimal();
            continue;
        }
        if (char() === "B") {
            fetchHexCommandBinary();
            continue;
        }
        if (char() === "$") {
            fetchReplace();
            continue;
        }
        if (/^\s$/g.test(char())) {
            next();
            continue;
        }
        error(`Unknown character: ${char()}`);
    }
    ticks.forEach((value, key, map) => {
        console.log(`Channel ${key} ticks: ${value}, Loop point: ${introTicks.get(key) || 0}`);
    });
    return allData;
}
