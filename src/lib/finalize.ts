import { readFileSync, writeFileSync } from "node:fs";
import * as url from "node:url";
import path from "node:path";
import { SongData } from "../types/song-data";
import { TrackData } from "../types/track-data";
import songRendererHect from "./song-renderer-hect";

const basePathInternal = url.fileURLToPath(import.meta.url);

const instrumentAddr = 0x2200;
const trackPointerAddr = 0x2304;
const trackStartAddr = 0x2314;

export interface FinalizeOptions {
    trackData: TrackData
    songData: Map<number | string, SongData[]>
    basePath: string
}

export default function finalize({ trackData, songData, basePath }: FinalizeOptions) {
    let trackApplyAddr = trackStartAddr;
    const template = readFileSync(path.join(basePathInternal, "../../../assets/template.spc"));
    // write track
    for (let i = 0; i < 8; i++) {
        const songDataTrack = songData.get(i);
        if (!songDataTrack) {
            continue;
        }
        const channelTrackStartAddr = trackApplyAddr;
        template.writeUint16LE(channelTrackStartAddr - 0x100, trackPointerAddr + i * 2);

        // song init
        if (i === 0) {
            template.set([0xFF, 0x05, 0xD0, 0x21], trackApplyAddr);
        } else {
            template.set([0xFF, 0x05, 0xE0, 0x21], trackApplyAddr);
        }
        trackApplyAddr += 4;

        // song binary
        const { binary, loopPoint, addressAppendList } = songRendererHect(songDataTrack);
        template.set(binary, trackApplyAddr);

        // handle address ref
        for (let i1 = 0; i1 < addressAppendList.length; i1++) {
            const e = addressAppendList[i1];
            const original = template.readUInt16LE(trackApplyAddr + e);
            template.writeUint16LE(original + trackApplyAddr - 0x100, trackApplyAddr + e);
            // console.log(original + " => " + (original + trackApplyAddr));
        }
        trackApplyAddr += binary.length;

        // jump back command
        template.set([0xFF, 0x07], trackApplyAddr);
        trackApplyAddr += 2;

        // jump back address (track start position + loop point + song init cmd length)
        template.writeUint16LE(channelTrackStartAddr - 0x100 + loopPoint + 4, trackApplyAddr);
        trackApplyAddr += 2;
    }
    console.log(`Music insert size: $${(trackApplyAddr - trackStartAddr).toString(16).padStart(4, "0")} bytes`);

    // Prepare for writing Instrument & BRR Sample
    // console.log(trackData);
    const sampleMap = new Map<string, Buffer>();
    trackData.instruments.forEach((e) => {
        sampleMap.set(e.fileName, readFileSync(path.join(basePath, e.fileName)));
    });
    const samplePairs = [...sampleMap];
    const instruments = trackData.instruments.map((e) => [samplePairs.findIndex((f) => f[0] === e.fileName), ...e.param]);
    // console.log(instruments);

    // Writing BRR Sample
    const samplePointerAddr = Math.ceil(trackApplyAddr / 0x100) * 0x100;
    template.writeInt8(Math.floor(samplePointerAddr / 0x100) - 1, 0x1015D);
    trackApplyAddr = samplePointerAddr + samplePairs.length * 4;

    samplePairs.forEach((e, i) => {
        const loopPoint = e[1].readUInt16LE(0);
        const data = new Uint8Array(e[1]).slice(2);

        // Writing pointer
        template.writeUint16LE(trackApplyAddr - 0x100, samplePointerAddr + i * 4);
        template.writeUint16LE(trackApplyAddr + loopPoint - 0x100, samplePointerAddr + i * 4 + 2);

        // Writing data
        template.set(data, trackApplyAddr);
        trackApplyAddr += data.length;
    });

    // Writing instruments
    let position = instrumentAddr;
    instruments.forEach((e) => {
        // console.log(e);
        template.set(e, position);
        position += 6;
    });

    // Writing metadata
    const textEncoder = new TextEncoder();
    template.set(textEncoder.encode(trackData.spc.song).slice(0, 32), 0x2E);
    template.set(textEncoder.encode(trackData.spc.game).slice(0, 32), 0x4E);
    template.set(textEncoder.encode(trackData.spc.comment).slice(0, 32), 0x7E);
    template.set(textEncoder.encode(trackData.spc.artist).slice(0, 32), 0xB1);
    template.set(textEncoder.encode(trackData.spc.length.toString().padStart(3, "0")), 0xA9);
    template.set(textEncoder.encode(trackData.spc.fadeout.toString().padStart(5, "0")), 0xAC);

    writeFileSync("./output.spc", template);
}
