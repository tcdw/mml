import { SongData, SongNoteData, SongRepeatEndData } from "../types/song-data";
import trackOptimizer from "./track-optimizer";

// 定义一个函数，输入一个数字，输出一个数组或者一个数字
function splitNumber(num: number): number[] {
    // 如果数字小于或等于96，直接返回这个数字
    if (num <= 96) {
        return [num];
    }
    // 否则，创建一个空数组，用来存放分割后的数字
    const result: number[] = [];
    // 从96开始循环，递减1，直到0
    for (let i = 96; i > 0; i--) {
        // 如果当前的数字小于或等于剩余的数字，就把它加入到数组中，并更新剩余的数字
        if (i <= num) {
            result.push(i);
            num -= i;
        }
        // 如果剩余的数字为0，就说明已经分割完毕，跳出循环
        if (num === 0) {
            break;
        }
    }
    // 返回分割后的数组
    return result;
}

// 测试一下函数的效果
// console.log(splitNumber(100)); // [96, 4]
// console.log(splitNumber(97)); // [96, 1]
// console.log(splitNumber(96)); // 96
// console.log(splitNumber(95)); // 95
// console.log(splitNumber(200)); // [96, 96, 8]

export default function songRendererHect(data: SongData[]) {
    const newData: SongData[] = [];
    const addressAppendList: number[] = [];
    trackOptimizer(data).forEach((e) => {
        if (e.type === "note" && e.length > 96) {
            const builtLen = splitNumber(e.length);
            const builtNote = builtLen.map((f, i) => ({
                type: "note",
                length: f,
                pitch: i === 0 ? e.pitch : -2,
                noteParam: e.noteParam,
            } as SongNoteData));
            newData.push(...builtNote);
        } else {
            newData.push(e);
        }
    });

    const binary: number[] = [];
    let currentNoteParam = -1;
    let currentNoteLength = -1;
    let loopPoint = 0;
    let loopStartPoint = 0;
    newData.forEach((e, i) => {
        switch (e.type) {
            case "repeat-start": {
                let j = i;
                while (newData[j].type !== "repeat-end") {
                    j++;
                }
                const loopTime = (newData[j] as SongRepeatEndData).param;
                binary.push(0xFF, 0x08, loopTime);
                loopStartPoint = binary.length;
                break;
            }
            case "repeat-end":
                // console.log(loopStartPoint);
                binary.push(0xFF, 0x09, loopStartPoint & 0xff, loopStartPoint >> 8);
                addressAppendList.push(binary.length - 2);
                break;
            case "loop-point":
                loopPoint = binary.length;
                currentNoteParam = -1;
                currentNoteLength = -1;
                break;
            case "hex-command":
                binary.push(e.param);
                break;
            case "note":
                if (currentNoteLength !== e.length && currentNoteParam !== e.noteParam) {
                    binary.push(e.length);
                    currentNoteLength = e.length;
                    binary.push(e.noteParam);
                    currentNoteParam = e.noteParam;
                } else if (currentNoteParam !== e.noteParam) {
                    binary.push(e.length);
                    currentNoteLength = e.length;
                    binary.push(e.noteParam);
                    currentNoteParam = e.noteParam;
                } else if (currentNoteLength !== e.length) {
                    binary.push(e.length);
                    currentNoteLength = e.length;
                }
                switch (e.pitch) {
                    case -1: {
                        binary.push(0xC9);
                        break;
                    }
                    case -2: {
                        binary.push(0xC8);
                        break;
                    }
                    default: {
                        binary.push(e.pitch + 116);
                        break;
                    }
                }
                break;
            case "instrument":
                binary.push(0xE0);
                binary.push(e.param);
                break;
            case "volume":
                binary.push(0xED);
                binary.push(e.param);
                break;
            case "global-volume":
                binary.push(0xE5);
                binary.push(e.param);
                break;
            case "pan":
                binary.push(0xE1);
                binary.push(e.param);
                break;
            case "tempo":
                binary.push(0xE7);
                binary.push(e.param);
                break;
            case "vibrato":
                binary.push(0xE3);
                binary.push(...e.param);
                break;
            default:
                break;
        }
    });
    return {
        binary,
        loopPoint,
        addressAppendList,
    };
}
