import { parse } from "yaml";
import { TrackData } from "../types/track-data";

type RawData = {
    instruments: Array<[string, number, number, number, number, number]>
    spc: {
        song?: string
        game?: string
        artist?: string
        comment?: string
        length?: number
        fadeout?: number
    }
    macro: Record<string, string>
};

export default function metaReader(data: string) {
    const rawData: RawData = parse(data);
    const trackData: TrackData = {
        tracks: [],
        instruments: [],
        instrumentNameMap: new Map(),
        macro: rawData.macro,
        spc: {
            song: rawData.spc.song || "",
            game: rawData.spc.game || "",
            artist: rawData.spc.artist || "",
            comment: rawData.spc.comment || "",
            length: Math.floor(rawData.spc.length || 300),
            fadeout: Math.floor(rawData.spc.fadeout || 100000),
        },
    };
    rawData.instruments.forEach((e) => {
        trackData.instruments.push({
            fileName: e[0],
            customName: "",
            param: [e[1], e[2], e[3], e[4], e[5]],
        });
    });
    return trackData;
}
