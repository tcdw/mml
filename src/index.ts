#!/usr/bin/env bun

import fs from "node:fs";
import songReader from "./lib/song-reader";
import metaReader from "./lib/meta-reader";
import finalize from "./lib/finalize";

const songName = process.argv[2];

const trackData = metaReader(fs.readFileSync(`./local_test/${songName}.yaml`, { encoding: "utf-8" }));
const songData = songReader(fs.readFileSync(`./local_test/${songName}.mml`, { encoding: "utf-8" }), {
    allowSwitchTrack: true,
    allowReplace: true,
    replaceMap: trackData.macro,
    barSize: 192,
});
finalize({
    trackData,
    songData,
    basePath: `./local_test/${songName}.samples`,
});
